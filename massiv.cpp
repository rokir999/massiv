﻿#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
using namespace std;
#include <ctime>


int main()
{
	setlocale(LC_ALL, "rus");
	int n, m, N, sum = 0;
	cout << "Введите значение N: ";
	cin >> N;
	time_t t;
	time(&t);
	int k = (localtime(&t)->tm_mday) % N;
	cout << "Введите размерность массива: ";
	cin >> n >> m;
	int** a = new int* [n];
	for (int i = 0; i < n; i++)
		a[i] = new int[m];
	cout << "Введите элементы массива: \n";
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			cin >> a[i][j];
			if (i == k) sum += a[i][j];
		}	
	}
	cout << "Сумма элементов строки " << k << " = " << sum;
	
}

